FROM node:boron

MAINTAINER Will OBrien

COPY . /src

WORKDIR /src

RUN npm install

EXPOSE 3000

CMD ["node", "rebuild"]
CMD ["npm", "start"]
