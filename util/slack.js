

//Create an options object for the Slack http call
function createSlackOptions(query){
  var marketCurr = query.split('-')[1];
  console.log( 'from slackOptions: '+ marketCurr);
  var token = 'xoxp-241499287379-241499287683-240944389024-000dfdbec3b436f60b05ad6caea64930';
  var urlOptions = {
    method: 'GET',
    uri: '',
    json:  true,
  }

  var str = '?token=' + token + '&query=' + marketCurr + '&pretty=1';
  urlOptions.uri = 'https://slack.com/api/search.all' + str; 
  return urlOptions;
}

//Parses the Slack JSON response 
function parseSlackRsp(rsp){
  var text = '';
  if(rsp.messages.total > 0){
    for(var i = 0; i < rsp.messages.matches.length; i++){
      text += '\"' + rsp.messages.matches[i].text + '\" </br></br>';
    }
  } else {
    text= "No Results.";
  }
  console.log(text);
  return text;
}

//Adds Slack text to the main object
function buildSlackObject(data, object){
  object.slack = parseSlackRsp(data);
  console.log('build slack: ' + object.slack);
  return object;
}

//exports methods for use in crypto.js
module.exports = {createSlackOptions : createSlackOptions, parseSlackRsp : parseSlackRsp,
   buildSlackObject : buildSlackObject }