
//Appends data to the main object
function buildSentimentObject(data, object){
  object.sentiment = data[0].documentSentiment;
  return object;
}

//exports method for use in crypto.js
module.exports = {buildSentimentObject : buildSentimentObject}