
//Create an options object for the Bittrex http call
function createBittrexOptions(query,market,type) {
  var urlOptions = {
      method: 'GET',
      uri: '',
      json: true,
  }
  var str = query +'?market=' + market +'&type=' + type;
  urlOptions.uri = 'https://bittrex.com/api/v1.1/public/' + str;
  return urlOptions;
}

//Parses the Bittrex JSON response for index.js
function parseIndexMarketsRsp(rsp) {
  var str = '';

  for(var i =0; i < rsp.result.length; i++){
    if(rsp.result[i].MinTradeSize <= .00000001 && rsp.result[i].BaseCurrency == "BTC"){
      str += '<option value="'+ rsp.result[i].MarketName + '">' + 
        rsp.result[i].BaseCurrencyLong + ' - ' + rsp.result[i].MarketCurrencyLong + '</option>\n'
    }
  }
  return str;
}

//Parses the Bittrex JSON response 
function parseMarketsRsp(rsp) {
  
  var market = {};
  market.name = rsp.result[0].MarketName;
  market.last =  rsp.result[0].Last.toFixed(8);
  market.yesterday = rsp.result[0].PrevDay.toFixed(8); 
  market.difference = (rsp.result[0].Last - rsp.result[0].PrevDay).toFixed(8);

  return market;
}

//Adds Bittrex object to the main object
function buildBittrexObject(data){
  let newObject = {
    bittrex: parseMarketsRsp(data),
    slack: {}
  }
  return newObject;
}


//exports methods for use in crypto.js and index.js
module.exports = {createBittrexOptions : createBittrexOptions, parseIndexMarketsRsp : parseIndexMarketsRsp,
  parseMarketsRsp : parseMarketsRsp, buildBittrexObject : buildBittrexObject }