# Crypto-Feels#

Crypto-feels is a Node/Express app built for a QUT Cloud-Computing Project by William O'Brien.

The app allows you to choose a crypto-currency pair from the Bittrex exchange, scan a message board for mentions of that currency and then analyse the sentiments of those messages.

It is currently hardcoded to use a test Slack board.

### Contact ###

willobrien@live.com