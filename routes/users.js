var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  // res.send('respond with a resource');
  d = new Date();
  dateNow = d.toString()
  res.render('users', {title : "Bittrex User", date : d.toString()});
});

module.exports = router;
