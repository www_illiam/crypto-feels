var express = require('express');
var router = express.Router();
var https = require('https');
var bittrex = require("../util/bittrex")



/* GET home page. */
router.get('/', function(appReq, appRes, next) {


    //Create options for first https cal to Bittrex
    var options = {
      hostname: 'bittrex.com',
      port: 443,
      path: '/api/v1.1/public/getmarkets',
      method: 'GET'
    }

    //Https call to Bittrex to populate the dropdown menu
    //All future calls are made via the request-promises API
    //This was built before I found out how to make my life easier
    var bittReq = https.request(options, function(bittRes) {
    
      var body = [];
      bittRes.on('data',function(chunk) {
          body.push(chunk);
      });
      bittRes.on('end', function() {
          var bodyString = body.join('');
          var rsp = JSON.parse(bodyString);
          var formattedString = bittrex.parseIndexMarketsRsp(rsp);
          appRes.render('index', {title : "CRYPTO-FEELS", format: formattedString});
      });   
  });

  bittReq.on('error', (e) => {
    console.error(e);
  });
  
  bittReq.end();
  

});

module.exports = router;
