"use strict";
var express = require('express');
var router = express.Router();
var bittrex = require("../util/bittrex")
var slack = require("../util/slack")
var natlan = require("../util/naturallanguage")
var app = express();
var request = require('request-promise')
var language = require('@google-cloud/language')({
  projectId: 'cab432project1',
  keyFilename: 'util/cab432project1-db13893fe8c9.json'
});


router.get('/', function(appReq, appRes, next) {

  //Create the options for the call to Bittrex
  var bittrexOptions = bittrex.createBittrexOptions('getmarketsummary',appReq.query.select,'both');

  //function to analyse the text from the Slack response
  let analyseText = function(response){
    var document = {
      content: response.slack,
      type: 'PLAIN_TEXT'
    };
    return language.analyzeSentiment({'document': document})
      .then(results => natlan.buildSentimentObject(results, response))
      .catch((err) => {
        console.error('ERROR:', err);
      });
    
  }

  //Call to Bittrex search with the users chosen market
  let getBittrex = function (bittrexOptions){
    return request(bittrexOptions).then(results => bittrex.buildBittrexObject(results))
    .catch((err) => {
      console.error('getBittrex ERROR:', err);
    });
  }

  //Call to Slack search with the chosen Bittrex market as the keyword
  let searchSlack = function (response){
    console.log(response.bittrex.name);
    return request(slack.createSlackOptions(response.bittrex.name)).then(results => slack.buildSlackObject(results, response))
    .catch((err) => {
      console.error('searchSlack ERROR:', err);
    });
  }

  //Basic function to create a readable output for the sentiment analysis
  let translateSentiment = function(response){
    let object = {
      magnitude: '',
      score: ''
    }
    if(!response.slack.includes("No Results")){
      switch(true) {
        case (response.sentiment.score >= -0.15 && response.sentiment.score <= 0.15) :
          object.score = 'Neutral';
          break;
        case (response.sentiment.score <= -0.1):
          object.score = 'Unhappy';
          break;
        case (response.sentiment.score >= 0.1):
          object.score = 'Happy';
          break;
        default:
          object.magnatude = 'Not';
          object.score = 'Available';
      }
      if(!object.score.includes('Neutral')){
        switch(true) {
          case (response.sentiment.magnitude >= 2.5) :
          object.magnitude = 'Unbelievably';
          break;
          case (response.sentiment.magnitude >= 1) :
            object.magnitude = 'Super';
            break;
          case (response.sentiment.magnitude >= 0.7):
            object.magnitude = 'Very';
            break;
          case (response.sentiment.magnitude >= 0.2):
            object.magnitude = 'Quite';
            break;
          default:
            object.magnitude = '';
        }
      } else {  
          object.magnitude = '';
      }

    } else {
      object.magnitude = 'Not';
      object.score = 'Available';
    }

    response.sentiment.readable = object;
    return response;
  }



  //Renders the crypto page with the data object
  let showPage = function(response){
    return appRes.render('crypto', {title: 'CRYPTO-FEELS', data: response});
  }

  // Chained promises to execute the functions in the correct order and pass the data object between them.
  getBittrex(bittrexOptions)
  .then(searchSlack)
  .then(analyseText)
  .then(translateSentiment)
  .then(showPage)
  .catch((err) => {
    console.error('main ERROR:', err);
  });


});

module.exports = router;
